#!/usr/bin/env python
import sys
import gc
import argparse
import os
import hashlib
import re
from bitarray import bitarray


def hashfunc(x):
  return int(hashlib.sha512(x).hexdigest(), 16)

SIMHASH_SIZE = 64
masks = [1 << i for i in xrange(SIMHASH_SIZE)]
SIZE_RATE = 1.25
VERBOSE = False

class Document:
    def __init__(self, filename, size, simhash):
        self.filename = fileName
        self.size = size
        self.simhash = simhash

    def __init__(self, fileName, text):
        self.size = len(text)
        self.fileName = fileName 
        words = [w.strip(",.[]{}())-+").lower() for w in re.split('[\n ]', text)]
        words = [w for w in words if w]
        bigrams = [None] * (len(words) - 1)
        for i in xrange(1, len(words)):
            bigrams[i-1] = "{} {}".format(words[i-1], words[i]) 
        self.__calcSimHash(bigrams)
        
    

    def __cmp__(self, other):
        return  self.size- other.size

    def __str__(self) :
        return "{} {} {}".format(self.filename, self.size, self.simhash)

    def __calcSimHash(self, features):
        hashs = [hashfunc(w) for w in features]
        v = [0]*SIMHASH_SIZE

        for h in hashs:
            for i in xrange(SIMHASH_SIZE):
                v[i] += 1 if h & masks[i] else -1
        self.simhash = bitarray(SIMHASH_SIZE)
        self.simhash.setall(False)
        for i in xrange(SIMHASH_SIZE):
            if v[i] >= 0:
                self.simhash[i] = 1
	
    def distance(self, other):
        return (self.simhash ^ other.simhash).count()


class DuplicateFinder:
    def __init__(self, path, n):
        self.n = n
        self.path = path
        self.documents = None
        self.distances = [0]*(SIMHASH_SIZE + 1)

    def run(self):
        dirList = os.listdir(self.path)
        
        i = 0
        self.documents = []
        
        for item in dirList:
            fileName = os.path.join(self.path,item)
            if os.path.isfile(fileName) :
                with open (fileName, "r") as docFile:
                    text = docFile.read()
                    tokens = text.split('\n', 1)
                    text = tokens[1].strip() if len(tokens) > 1 else ""
                    if len(text) > 10:
                        self.documents.append(Document(item, text))
                    if VERBOSE:
                        print i      
                    i += 1

        print "Sorting by size"
        self.documents.sort()
        print "Clasterizing"
        self.__clasterize()
    
    def __clasterize(self):
        isPicked = bitarray(len(self.documents))
        isPicked.setall(False)
        self.groups = []
        for i in xrange(len(self.documents)):
            if isPicked[i]:
                continue
            if VERBOSE:
                print i
            group = {'main': self.documents[i], 'duplicates': []}
            for j in xrange(i + 1, len(self.documents)):
                if not isPicked[i] and not isPicked[j]:
                    
                    if self.documents[i].size * SIZE_RATE < self.documents[j].size:
                        break

                    distance = self.documents[i].distance(self.documents[j])
                    self.distances[distance] += 1;
                    if distance <= self.n:
                        isPicked[j] = True
                        group['duplicates'].append(self.documents[j])

            self.groups.append(group)
        self.groups.sort(key=lambda x: -len(x['duplicates']))



def main():
    script_name = os.path.basename(sys.argv[0])
    parser = argparse.ArgumentParser(description='Find semiduplicates.')
    parser.add_argument('dir', metavar='DIR', help='directory with documents')
    parser.add_argument('-out-dir', metavar='OUT-DIR', default=".", help='output directory')
    parser.add_argument('-n', type=int, default=15, help='number of distinct bits in simhash')
    parser.add_argument('--top10', action="store_true", help='show top 10 groups')
    parser.add_argument('--verbose', action="store_true", help='verbose output')
   
    args = parser.parse_args()
    print args
    global VERBOSE 
    VERBOSE = args.verbose

    df = DuplicateFinder(args.dir, args.n)
    df.run()
    groups = df.groups
    print "Groups count:", len(groups)

    if not os.path.isdir(args.out_dir):
        try:
            os.makedirs(args.out_dir)
        except:
            print >> sys.stderr, 'Could not create: ', args.out_dir
            return

    if args.top10:
        with open (os.path.join(args.out_dir, "top10"), "w") as fout:
            for i in xrange(min(10, len(groups))):
                with open (os.path.join(args.dir, groups[i]['main'].fileName), "r") as docFile:
                    text = docFile.read()
                    tokens = text.split('\n', 1)
                    title = tokens[0].strip()
                    text = tokens[1].strip() if len(tokens) > 1 else ""
                fout.write("Group {} SIZE = {}:\n".format(i, len(groups[i]['duplicates'])+ 1))
                fout.write("---Main: filename = {}\ttitle = {}\n{}\n\n".format(groups[i]['main'].fileName, title, text))
    
    with open (os.path.join(args.out_dir, "groupsSizes"), "w") as fout:
        for group in groups:
            fout.write(str(len(group['duplicates']) + 1) + '\n')
    
    with open (os.path.join(args.out_dir, "distances"), "w") as distancesFile:
        for dist in df.distances:
            distancesFile.write(str(dist) + '\n')



if __name__ == '__main__':
    main()