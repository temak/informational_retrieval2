#!/usr/bin/env python

import numpy as np
import os
import sys
import argparse
import matplotlib.pyplot as plt


def main():
    script_name = os.path.basename(sys.argv[0])
    parser = argparse.ArgumentParser(description='Draw histograms')
    parser.add_argument('dir', metavar='DIR5', help='directory with documents')
    parser.add_argument('-dist', action="store_true", help='draw distances distribution histogram')

    #parser.add_argument('-out-dir', metavar='OUT-DIR', default=".", help='output directory')
   
    args = parser.parse_args()
    print args

    sizes = None
    with open (os.path.join(args.dir, "groupsSizes"), "r") as f:
        sizes = [int(line.rstrip('\n')) for line in f]
    hist, bin_edges = np.histogram(sizes, bins = range(10), density=True)
    print hist, bin_edges
    plt.bar(bin_edges[:-1], hist, width = 1)
    plt.xlim(min(bin_edges), max(bin_edges))
    plt.title('Groups Sizes distribution')
    plt.xlabel('Size')
    plt.ylabel('Probability')
    plt.show()


    if args.dist:
        distances = None
        with open (os.path.join(args.dir, "distances"), "r") as f:
            distances = [int(line.rstrip('\n')) for line in f]
        hist, bin_edges = np.histogram([], bins = range(66), density=True)
        total = sum(distances)
        print "total=", total
        distances = [float(d) / total for d in distances]
        print distances, bin_edges
        plt.bar(bin_edges[:-1], distances, width = 1)
        plt.xlim(min(bin_edges), max(bin_edges))
        plt.title('Distances distribution') 
        plt.xlabel('Distance')
        plt.ylabel('Probability')
        plt.show()


if __name__ == '__main__':
    main()